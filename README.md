## What is this repository for? ##



Personally, I hate installing random software on my beautiful pristine mac (I'm actually writing this on a windows laptop so I don't debase my mac), and so should you, the road to hell is paved with "useful" software. Between app errors, broken dependencies, borked updates, misconfigurations, and half installed tools, too much software spoils the broth, and can become the operating system equivalent of an immune disorder attacking the very host that keeps the half baked crap alive. Just say no, keep it clean, use protection, install the invasive flora in a VM that can be blown away and recreated in a couple of commands. Oh, and make it easier for those who need access to the same tools after you. May future developers stand on the shoulders of automations. Hail devsandpit!

NOTE: It's "dev sandpit", not "devs and pit".

This repo creates a local VM with useful pre installed developer tools and services. The core benefits being:

* Automate the installation of tools
* Keep your mac clean - don't pollute your os with myriad software
* Easily re-create de tools "at a click" in failure scenarios
* Keep versions and os consistent
* Document and support tool installation from single source of truth
* Easily share and contribute to enable every to pool their skills together
* Track down and isolate errors quickly with all participants having common, consistent environments


#### what gets installed? ####

Currently installed:

* java8
* awe-cli
* nodejs & npm
* cassandra db
* ansible
* terraform
* jetty java application server
* docker, docker-compose, docker-machine
* an intro page at http://localhost:8008


TODO:

* alpine minimal VM
* package in to vagrant box
* other stuff


### How do I get set up? ###

#### Dependencies

Here's what you need installed on you mac before the next step to get it up and running:

  * **vagrant**: https://www.vagrantup.com/downloads.html
  * **virtual box**: https://www.virtualbox.org/wiki/Downloads
  * **ansible**: (ideally install via brew: http://brew.sh) ==> ```brew install ansible```

#### Setup instructions

```
git clone https://gawainh@bitbucket.org/gawainh/devsandpit.git
cd devsandpit
make build
```

Now drive your browser to this very-retro-times-new-roman-wtf-is-css welcome page:

http://localhost:8008

#### Configuration

Curently the VM is configured to use 4GB of RAM, if that's too little (if it's too much you're out of luck buddy), change this line below in the file ```./Vagrantfile``` to something more suitable.

```
  v.memory = 4096
```


#### syncing the latest updates

```
git pull
make reprovision
```


 That didn't work as expected? Blow it away as below:


```
git pull
make clean
```



#### How to run tests

```
make test
```

#### vagrant box deployment

```
make publish
```



## Contribution guidelines ##


Uncle Sandpit needs YOUR help!

Please feel free/obliged to add new tasks in ``` provisioning/tasks``` or ideally new roles for more complex & flexible components as ansible roles under ```roles/<insert-your-software-package-here>```

#### Learning ansible

This is a great intro guide and will get you up and running very quickly. Just decide if what you want to do would be a task or a role (tasks are typically blanket and will be used on every VM (e.g.: install java & awe-cli). Roles are for optional and more complicated components that could be used standalone at a later stage.

https://adamcod.es/2014/09/23/vagrant-ansible-quickstart-tutorial.html

#### Writing tests
#### Code review
#### Other guidelines


## Other Stuff ##

#### make command line completion

If you type make <tab> <tab> you should get command line completion for make options. If not, fear not, instructions below:

```
$ brew install bash-completion
$ brew tap homebrew/completions
```
After you run that first command, in typical brew fashion, it will request that you add the following tidbit to your ~/.bash_profile. Don�t forget this part. It�s critical!
```
if [ -f $(brew --prefix)/etc/bash_completion ]; then
. $(brew --prefix)/etc/bash_completion
fi
```

## Who do I talk to? ##

#### Repo owner or admin

gawain.hammond@wipro.com
